#!/bin/bash

# Bash Color
green='\033[01;32m'
red='\033[01;31m'
blink_red='\033[05;31m'
restore='\033[0m'

clear

# Resources
THREAD="-j5"
KERNEL="zImage"
DEFCONFIG="gee_defconfig"

# Kernel Details
BASE_OAK_VER="Radioactive-GEEHRC"
VER="-r1.0"
OAK_VER="$BASE_OAK_VER$VER"

# Vars
export LOCALVERSION=-`echo $OAK_VER`
export ARCH=arm
export SUBARCH=arm
export ENABLE_GRAPHITE=true

# Paths
KERNEL_DIR=`pwd`
REPACK_DIR="${HOME}/AnyKernel2"
ZIP_MOVE="${HOME}/Kernels"
ZIMAGE_DIR="${HOME}/Radioactive-KERNEL/arch/arm/boot"
DB_FOLDER="${HOME}/Kernels/BasketBuild"

# Functions
function clean_all {
		rm -rf $REPACK_DIR/zImage
		make clean && make mrproper
}

function make_kernel {
		make $DEFCONFIG
		make nconfig
		make $THREAD
		cp -vr $ZIMAGE_DIR/$KERNEL $REPACK_DIR
		find . -name '*ko' -exec cp '{}' $REPACK_DIR/modules/ \;
}

function make_zip {
		cd $REPACK_DIR
		zip -9 -r `echo $OAK_VER`.zip .
		mv  `echo $OAK_VER`.zip $ZIP_MOVE
		cd $KERNEL_DIR
}

function copy_dropbox {
		cd $ZIP_MOVE
		cp -vr  `echo $OAK_VER`.zip $DB_FOLDER
		cd $KERNEL_DIR
}

DATE_START=$(date +"%s")

echo -e "${green}"
echo "Radioactive Kernel Builder:"
echo

echo "---------------"
echo "Kernel Version:"
echo "---------------"

echo -e "${red}"; echo -e "${blink_red}"; echo "$OAK_VER"; echo -e "${restore}";

echo -e "${green}"
echo "-----------------"
echo "Making Radioactive Kernel:"
echo "-----------------"
echo -e "${restore}"

while read -p "Please choose your option: [1]clean-build / [2]dirty-build / [3]abort " cchoice
do
case "$cchoice" in
	1 )
		echo -e "${green}"
		echo
		echo "[..........Cleaning up..........]"
		echo
		echo -e "${restore}"
		clean_all
		echo -e "${green}"
		echo
		echo "[....Building `echo $OAK_VER`....]"
		echo
		echo -e "${restore}"
		make_kernel
		echo -e "${green}"
		echo
		echo "[....Make `echo $OAK_VER`.zip....]"
		echo
		echo -e "${restore}"
		make_zip
		echo -e "${green}"
		echo
		echo "[.....Moving `echo $OAK_VER`.....]"
		echo
		echo -e "${restore}"
		copy_dropbox
		break
		;;
	2 )
		echo -e "${green}"
		echo
		echo "[....Building `echo $OAK_VER`....]"
		echo
		echo -e "${restore}"
		make_kernel
		echo -e "${green}"
		echo
		echo "[....Make `echo $OAK_VER`.zip....]"
		echo
		echo -e "${restore}"
		make_zip
		echo -e "${green}"
		echo
		echo "[.....Moving `echo $OAK_VER`.....]"
		echo
		echo -e "${restore}"
		copy_dropbox
		break
		;;
	3 )
		break
		;;
	* )
		echo -e "${red}"
		echo
		echo "Invalid try again!"
		echo
		echo -e "${restore}"
		;;
esac
done

echo -e "${green}"
echo "-------------------"
echo "Build Completed in:"
echo "-------------------"
echo -e "${restore}"

DATE_END=$(date +"%s")
DIFF=$(($DATE_END - $DATE_START))
echo "Time: $(($DIFF / 60)) minute(s) and $(($DIFF % 60)) seconds."
echo