# Radioactive Kernel

Radioactive is Lollipop Kernel for international LG Optimus G. This Kernel is based on MiRaGe Mako Kernel by mrg666. Meant is for Lollipop 5.1 ROMs, but it should work on Android 5.0 as well.

With this Kernel I am learning about Linux/Android Kernel developement, so it probably won't be published really soon. I will use it more or less for personal learning repository. But maybe one day this Kernel will be born as relase and shared, till then, you can try to build it yourself. :)

This Kernel is going to have implemented a lot of functions, but probably most of time this functions will be buggy/risky until i will fix them.

This Kernel will be build with Linaro Toolchain 4.9.3, with Graphite, O3 and pipe optimizations (Highly optimized Kernel). So, when i come to point when this Kernel will be stable enough for distribution (It should be really smooth because of all optimizations and customizations) I will share it.

I'm going to implement Overclock too, so this will be OVERCLOCKING Kernel, what means that it has possibility of broking your hardware (Pushing CPU/GPU Freqency Higher than it is supposed to be) and it will be EXTREME overclocking Kernel, so I can't anticipate how your hardware is gonna act while overclocking. But you can still turn overclocking off in any Kernel Managment app. This Kernel will also have UNDERCLOCKING implemented, so CPU & GPU Frequency can go lower than it is supposed to go. This means more batter life, but this function can be unstable as overclocking can be. This can also be turned off in any Kernel Managment app.

Undervolting will be implemented too, so CPU will get less voltage as it is supposed to get. This technically means more battery life, but it can be unstable, too.